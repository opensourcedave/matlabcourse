dname='ADDquestionaireResponses';
mkdir(dname)
for i = 1:50
    m=normrnd(6,.5,1);
    R=poissrnd(m,20,1);
    R(R>9)=9;
    R(R<1)=1;
    subj=['TST' num2str(i)];
    fname=[ dname filesep subj '_addQs'];
    save(fname,'R','subj');
end