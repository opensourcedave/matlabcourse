function [b,ind] = isalpha(in)
%determine whether string is contains only letters
  ind=find(~ismember(in,alphaV));
  if isempty(ind)
      b=1;
  else
      b=0;
  end
end


function A = alphaV()
  %List of valid characters
  A=['abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'];
  A=A(:);
end