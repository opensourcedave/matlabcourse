function x = quadratic(a,b,c)
% some simple help
x(1) = -b + sqrt(b^2-4*a*c)/(2*a);
x(2) = -b - sqrt(b^2-4*a*c)/(2*a);
