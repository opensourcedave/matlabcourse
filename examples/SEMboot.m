function [SEM MM]=SEMboot(data,nBootS,bPlot)
% Bootstrapping to get Standard error of the mean
% be used for confidence intervals for the sample mean
if ~exist('data','var') || isempty(data)
    [data,I]=data_gen();
else
    I=[]
end

% BOOTSTRAP
M=zeros(length(data),1);
for i=1:nBootS
   y=datasample(data,length(data),'Replace',true);
   M(i)=mean(y);
end
SEM=std(M);
MM=mean(mean(M));
CI=[MM-SEM MM+SEM];

if bPlot
   Plot(data,M,SEM,CI,MM,I)
end
end

function []=Plot(data,M,SEM,CI,MM,I)
% HANDLE PLOTTING
    figure(1)
    subplot(1,2,1)
    hold off
    [counts,ctrs]=hist(data);
    bar(ctrs,counts)
    y=max(counts);
    Y=[y y];
    hold on
    plot(CI,Y,'r','LineWidth',5)
    plot(MM,y,'kd')
    title('Data')
    axis square
    hold off

    subplot(1,2,2)
    histogram(M);
    axis square
    name=['Mean: ' num2str(mean(data))  newline ...
                   'SEM:  ' num2str(SEM) ];
    if ~isempty(I)
        name=[name newline 'True = ' num2str(I)];
    end
    title(name);
end


function [data,I]=data_gen()
% Generates example data if you have noen
    I=rand(1)*3+5;
    data=normrnd(I,1,100,1);
    for i = 1:length(data)
        data(i)=data(i)+normrnd(0,abs(data(i))*1.36);
    end
end
