function [out] = isvar(varname)
% function [out] = isvar(varname)
% Performs the function of exist('var',varname) && isempty(varname)
% negate to check if a variable hasn't been defined yet.
% example calls:
%   A=[]; isvar('A')
%   clear A; isvar('A')
% CREATED BY DAVID WHITE

if ~ischar(varname)
   error('isvar: make sure to put your variable name in quotations!')
end

try
   var=evalin('caller',varname);
catch
  out=0;
  return
end

if isempty(var)
  out=0;
else
  out=1;
end

%handle structs
if isstruct(var)
    if isempty(fieldnames(var))
        out=0;
    else
        out=1;
    end
end

%handle cells
if iscell(var)
    if all(cellfun(@(x) isempty(x),var))
        out=0;
    else
        out=1;
    end
end
