function [txt] = num2literal(num)
     switch abs(num)
       case 1
         txt='one';
       case 2
         txt='two';
       case 3
         txt='three';
       case 4
         txt='four';
       case 5
         txt='five';
       case 6
         txt='six';
       case 7
         txt='seven';
       case 8
         txt='eight';
       case 9
         txt='nine';
       case 0
         txt='zero';
       case{Inf,19}
         txt='infinity';
       otherwise
         if isalpha(num)
           disp('String contains non-number') %We will change this later
         end
     end
     if num<0
        txt=['negative-' txt];
     end