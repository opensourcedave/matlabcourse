function [] = ADDquestionaire(subj);
if ~exist('subj','var') || isempty(subj)
    error('Requires subject initials.');
end
Q=txt2lines('addquestions.txt');
validRange=1:9;
clc
input(['Welcome.' ...
   newline 'Please rate your response with the following questions with numbers ' num2str(validRange(1)) ' to ' num2str(validRange(end)) '.' ...
   newline 'Press return to start questionaire: '],'s');
R=questionaire(Q,validRange);
dname='ADDquestionaireResponses';
mkdir(dname);
fname=[ dname filesep subj '_addQs'];
save(fname,'R','subj')
