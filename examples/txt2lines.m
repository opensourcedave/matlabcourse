function tlines = txt2lines(fname)
                    % Opens a text file, returns each line of the file as an element in a cell
    fid=fopen(fname);  %opens file
    tline = fgetl(fid);%reads file into variable
    tlines = cell(0,1);
    while ischar(tline)
        tlines{end+1,1} = tline;
        tline = fgetl(fid);
    end
    fclose(fid); %closes file
