function R = questionaire(Q,validRange)
%A questionaire that accepts responses in numbers
% Q          - Questions to ask
% validRange - range of numbers that are acceptable
% R          - Responses

assert(iscell(Q),'Questions are not in cell format.')
R=zeros(size(Q));

for i = 1:length(Q)
    clc %clear screen with every new question
    while true

        try
            %PROMPT FOR INPUT
            rsp=input([Q{i} ': '],'s');
            R(i)=str2double(rsp);

            %CHECK IF RESPONSE IS WITHIN RANGE
            fail=~ismember(R(i),validRange);
        catch
            fail=1;
        end

        if fail
            clc
            disp('Invalid response. Try again.')
        else
            break
        end

    end
end
clc
disp('Questionaire Complete. Thank you!')
