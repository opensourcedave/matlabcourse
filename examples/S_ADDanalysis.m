%ANALYSIS SCRIPT
try
    dname='ADDquestionaireResponses';
    cd(dname);
    files=dir;
    files={files.name};
    ind=contains(files,'mat');
    files=files(ind);
    scores=zeros(length(files),1);
    Subjs=cell(length(files),1);

    %PROCESS BY SUBJECT
    All=[];
    for i = 1:length(files)
        load(files{i});
        Subjs{i}=subj;
        S.(subj).R=R;
        scores(i)=sum(num2str(R));
        S.(subj).score=scores;
        S.(subj).num=i;
        All=[All; R];
    end

    %Z-SCORES
    zscores=zscore(scores);
    for i = 1:length(Subjs)
        S.(Subjs{i}).zscore=zscores(i);
    end

    figure(1)
    plot(zscores)
    title('ADD questionare Z-scores')
    xlabel('Subject number')
    ylabel('Z-Score')

    %Correlation
    n=randn(length(Subjs),1);
    rho=corr(scores,n);

    figure(2)
    plot(scores,n,'k.')
    title(['rho = ' num2str(rho)])

catch ME
    cd ..
    rethrow(ME)
end
cd ..